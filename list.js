var lists = [];
lists = getStoredLists();
var listId;
var id1;

//drag functions

function dragStart(e) {

  id1 = this.id;
  e.dataTransfer.effectAllowed = "move";

}
function dragOver(e) {
  e.preventDefault();
}

function dragDrop(e) {
  var id2 = this.id;
  var temp = lists[id1 - 1];
  lists[id1 - 1] = lists[id2 - 1];
  lists[id2 - 1] = temp;
  storeLists(lists);
  DeleteAllNodes();
  addListToDom();
}

//enter key event listner
function onEnter(event) {
  if ((event.code === 'Enter' || event.code === 'NumpadEnter')) {
    addListToArray();
    document.getElementById("AddedList").value = " ";
  }
}

//storing list in the local Storage

function storeLists(lists) {
  localStorage.lists = JSON.stringify(lists);
}

window.onload = function () {
  DeleteAllNodes();
  addListToDom();

}
//getting the stored lists

function getStoredLists() {
  if (!localStorage.lists) {
    // default to empty array
    localStorage.lists = JSON.stringify([]);
  }
  return JSON.parse(localStorage.lists);
}

function addListToArray() {
  var text = document.getElementById('AddedList').value;
  var dict = {};
  dict.key = text;
  dict.value = 0;
  lists.unshift(dict);
  storeLists(lists);
  DeleteAllNodes();
  addListToDom();
}

function addListToDom() {
  listId = 1;
  for (i = 0; i < lists.length; i++) {
    var newList = document.createElement("li");
    newList.draggable = "true";
    newList.ondragstart = dragStart;
    newList.ondragover = dragOver;
    newList.ondrop = dragDrop;
    newList.id = listId;
    listId++;
    var newP = document.createElement("p");
    if(lists[i].value == 0) {
      newP.className = 'style-list-1';
    }
    else {
      newP.className = 'style-list-2';
    }
    newP.onclick = MarkCompleted;
    var node = document.createTextNode(lists[i].key);

    // creating cross sign
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    span.onclick = deleteNode;

    //adding text to the ptag
    newP.appendChild(node);
    //adding ptag and span tag to list
    newList.appendChild(newP);
    newList.appendChild(span);
    var parent = document.getElementById('UnorderedList');
    parent.appendChild(newList);
  }
}

function DeleteAllNodes() {
  var list = document.getElementById('UnorderedList');
  while (list.hasChildNodes()) {
    list.removeChild(list.lastChild);
  }
}

function MarkCompleted() {
  var getId = event.target.parentNode.id;
  if (event.target.className === 'style-list-1') {
    event.target.className = 'style-list-2';
    console.log(getId + " " + lists[getId-1].value);
    lists[getId-1].value = 1;
  } else {
    event.target.className = 'style-list-1';
    lists[getId-1].value = 0;
  }
  storeLists(lists);
}

function deleteNode() {
  var getRemoveId = event.target.parentNode.id;
  lists.splice(getRemoveId - 1, 1);
  event.target.parentNode.parentNode.removeChild(event.target.parentNode);
  storeLists(lists);
}